<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
	'/',
	function () {
		return view('welcome');
	}
);

//Route::get('/setwebhook','SetWebHookController@set');
Route::get('/vk/auth', 'UserVkAuthController@auth');
Route::post(
	'/webhook/' . config('telegram.bot_token'),
	function () {
		/** @var Telegram\Bot\Objects\Update $update */
		$update = Telegram::commandsHandler(true);

		/** @var  \Telegram\Bot\Objects\Message $message */
		$message = $update->get('message');
		$text    = $message->getText();
		if (!preg_match('/^\/[0-9a-zA-Z]+/u', $text))
		{
			$command = new \App\Telegram\Command\HandleTextCommand();

			return $command->make(App::make(\Telegram\Bot\Api::class), [], $update);
		}

		return 'ok';
	}
);