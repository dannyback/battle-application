<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('battle', function () {
    $bm = new \App\Battle\Models\Battle();
    $battle = $bm->newQuery()->get()->first();

    dd($battle->redBattler);
});
