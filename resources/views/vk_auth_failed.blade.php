<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">

    <title>Рой баттл</title>

    <!-- Styles -->
    <style>
        html, body {
            font-size: 16px;
            font-family: 'Neucha', cursive;
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
        }

        #container {
            width: 100%;
            height: 100%;
            background-color: #ffd71c;
        }

        #message {
            width: 30%;
            height: auto;
            background-color: black;
            padding: 1.6em;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        #message p {
            color: black;
            background-color: #ffd71c;
            display: block;
            padding: 1.6em
        }

        #message b {
            font-weight: bold;
        }

    </style>
</head>
<body>
<div id="container">
    <div id="message">
        <p>Что-то пошло не так в этом мире, попробуй вернутся к боту и набери <b>/battle</b></p>
    </div>
</div>
</body>
</html>