<?php

namespace App\Console\Commands;

use App\Battle\Models\Battle;
use App\Telegram\Service\BattleService;
use Illuminate\Console\Command;
use Log;

class ManageBattle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'battle:manage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $battlesModel = new Battle();

        Log::useFiles(storage_path('logs/check.log'));

        $loop = \React\EventLoop\Factory::create();
        $loop->addPeriodicTimer(5, function () use ($battlesModel) {
            $battles = $battlesModel->newQuery()->get();

            if ($battles->count() == 1) {
                BattleService::manageBattle($battles->first());
            }
        });
        $loop->run();
    }
}
