<?php

namespace App\Console\Commands;

use App\Battle\Models\Battle;
use App\Battle\Models\Battler;
use App\Jobs\RegisterBattle;
use Illuminate\Console\Command;
use Log;

class CheckBattlers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'battle:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $battlersModel = new Battler();
        $battlesModel = new Battle();

        Log::useFiles(storage_path('logs/check.log'));

        $loop = \React\EventLoop\Factory::create();
        $loop->addPeriodicTimer(5, function () use ($battlersModel, $battlesModel) {
            $battlers = $battlersModel->newQuery()->orderBy('created_at')->limit(2)->get();
            $battles = $battlesModel->newQuery()->get();

            if ($battlers->count() > 1 && $battles->count() === 0) {
                RegisterBattle::dispatch($battlers);
            }
        });
        $loop->run();
    }
}
