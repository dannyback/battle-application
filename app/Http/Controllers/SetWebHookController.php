<?php

namespace App\Http\Controllers;

class SetWebHookController extends Controller
{
	/**
	 * @return \Response
	 */
	public function set()
	{
		/** @var \Telegram\Bot\Objects\Message $response */
		$response = \Telegram::setWebhook(['url' => config('telegram.bot_uri') . config('telegram.bot_token')]);

		$status = $response->get(0);

		return view('setwebhook', ['status' => $status ? 'ok' : 'nope']);
	}
}
