<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class UserVkAuthController extends Controller
{
	/**
	 * @return \Response
	 */
	public function auth(Request $request)
	{
		$code   = $request->input('code');
		$userId = $request->input('state');
		$client = new \GuzzleHttp\Client();

		try
		{
			$res = $client->get(
				'https://oauth.vk.com/access_token',
				[
					'query' =>
						[
							'client_id'     => config('app.vk_app_id'),
							'client_secret' => config('app.vk_app_secret'),
							'redirect_uri'  => 'https://trollbattle.ru/vk/auth/',
							'code'          => $code
						]
				]
			);
		}
		catch (ClientException $exception)
		{
			return view('vk_auth_failed');
		}

		if ($res->getStatusCode() == 200)
		{
			$authData = json_decode($res->getBody()->getContents(), true);
			$user     = new User(
				[
					'id'              => $userId,
					'name'            => '',
					'vk_token'        => $authData['access_token'],
					'vk_token_expire' => $authData['expires_in'],
					'info'            => ''
				]
			);
			$user->save();

			return view('vk_auth_success');
		}

		return view('vk_auth_failed');
	}
}
