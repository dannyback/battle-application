<?php

namespace App\Jobs;

use App\Battle\Models\Battle;
use App\Battle\Models\BattleWatcher;
use App\Battle\Models\Watcher;
use App\Telegram\Service\BattleService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class RegisterBattle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $battlers;

    public function __construct(Collection $collection)
    {
        $this->battlers = $collection;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $battleModel = new Battle();
        $watchersModel = new Watcher();
        $battleWatcherModel = new BattleWatcher();
        $watchers = $watchersModel->newQuery()->get();

        $battle = $battleModel->newInstance([
            'red_user_id' => $this->battlers[0]->user_id,
            'blue_user_id' => $this->battlers[1]->user_id,
        ]);

        $battle->save();

        foreach ($this->battlers as $battler) {
            $battler->delete();
        }

        foreach ($watchers as $watcher) {
            $battleWatcher = $battleWatcherModel->newInstance([
                'battle_id' => $battle->getKey(),
                'user_id' => $watcher->user_id,
            ]);
            $battleWatcher->save();
            $watcher->delete();
        }

        BattleService::startBattle($battle);
    }
}
