<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $fillable = ['id', 'name', 'vk_token', 'vk_token_expire', 'info', 'chat_id'];

	public $incrementing = false;

}
