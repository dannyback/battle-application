<?php

namespace App\Battle\Models;

use Illuminate\Database\Eloquent\Model;

class Battler extends Model
{
    protected $table = 'battlers';

    protected $fillable = ['user_id'];
}