<?php

namespace App\Battle\Models;

use Illuminate\Database\Eloquent\Model;

class Watcher extends Model
{
    protected $table = 'watchers';

    protected $fillable = ['user_id'];
}