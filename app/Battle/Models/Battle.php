<?php

namespace App\Battle\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Battle extends Model
{
    protected $table = 'battles';

    protected $fillable = [
        'red_user_id',
        'blue_user_id',
    ];

    /**
     * @return HasMany
     */
    public function watchers(): HasMany
    {
        return $this->hasMany(BattleWatcher::class, 'battle_id');
    }

    public function redBattler(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'red_user_id');
    }

    public function blueBattler(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'blue_user_id');
    }
}
