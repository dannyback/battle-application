<?php

namespace App\Telegram\Command;

use App\Battle\Models\Battler;
use App\Battle\Models\Watcher;
use App\User;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class WatchCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "watch";

	/**
	 * @var string Command Description
	 */
	protected $description = "Хочу быть зрителем";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		/** @var  \Telegram\Bot\Objects\Message $message */
		$message      = $this->getUpdate()->get('message');
		$chatId       = $message->getChat()->getId();
		$telegramUser = $message->getFrom();
		$userId       = $telegramUser->getId();

		$user = User::where('id', $userId)->first();
		if (is_null($user))
		{
			$user = new User(['id' => $userId, 'name' => $telegramUser->getUsername()]);
		}

		$user->chat_id = $chatId;
		$user->save();

		if (!is_null(Battler::where('user_id', $userId)->first()))
		{
			$this->replyWithMessage(
				[
					'text' => $user->name . ', ты будешь биться, зрителем стать уже не удасться=)! Ожидай...'
				]
			);
		}
		else if (!is_null(Watcher::where('user_id', $userId)->first()))
		{
			$this->replyWithMessage(
				[
					'text' => $user->name . ', ты уже и так зритель! Потерпи...'
				]
			);
		}
		else
		{
			$watcher = new Watcher(['user_id' => $userId]);
			$watcher->save();

			$this->replyWithMessage(
				[
					'text' => $user->name . ', ты теперь зритель! Идет подбор битвы для просмотра...'
				]
			);
		}
	}
}
