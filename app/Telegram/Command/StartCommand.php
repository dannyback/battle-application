<?php

namespace App\Telegram\Command;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "start";

	/**
	 * @var string Command Description
	 */
	protected $description = "Start команда для начала работы с ботом";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		$this->replyWithMessage(['text' => 'Рой баттл']);

		// This will prepare a list of available commands and send the user.
		// First, Get an array of all registered commands
		// They'll be in 'command-name' => 'Command Handler Class' format.
		$commands = $this->getTelegram()->getCommands();

		// Build the list
		$response = '';
		foreach ($commands as $name => $command)
		{
			$response .= sprintf('/%s - %s' . PHP_EOL, $name, $command->getDescription());
		}

		// Reply with the commands list
		$this->replyWithMessage(['text' => $response]);
	}
}
