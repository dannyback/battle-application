<?php

namespace App\Telegram\Command;

use App\Battle\Models\Battle;
use App\Battle\Models\BattleWatcher;
use App\User;
use Telegram\Bot\Commands\Command;

class VoteCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "vote";

	/**
	 * @var string Command Description
	 */
	protected $description = "Голосовать";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		/** @var  \Telegram\Bot\Objects\Message $message */
		$message      = $this->getUpdate()->get('message');
		$telegramUser = $message->getFrom();
		$userId       = $telegramUser->getId();

		$user = User::where('id', $userId)->first();
		if (!is_null($user))
		{
			$battleWatcher = BattleWatcher::where('user_id', $userId)->first();
			if (!is_null($battleWatcher))
			{
				$battleId = $battleWatcher->battle_id;
				/** @var Battle $battle */
				$battle = Battle::where('id', $battleId)->first();
				if ($arguments == 'red')
				{
					$battle->red_score++;
				}
				else
				{
					$battle->blue_score++;
				}

				$battle->save();
			}
		}
	}
}
