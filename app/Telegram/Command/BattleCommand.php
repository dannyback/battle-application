<?php

namespace App\Telegram\Command;

use App\Battle\Models\Battler;
use App\Battle\Models\Watcher;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class BattleCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "battle";

	/**
	 * @var string Command Description
	 */
	protected $description = "Хочу биться";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		/** @var  \Telegram\Bot\Objects\Message $message */
		$message      = $this->getUpdate()->get('message');
		$chatId       = $message->getChat()->getId();
		$telegramUser = $message->getFrom();
		$userId       = $telegramUser->getId();

		$user = User::where('id', $userId)->first();
		if (is_null($user) || $user->updated_at->addSeconds($user->vk_token_expire)->lt(Carbon::now()))
		{
			$this->replyWithMessage(
				[
					'text' => 'Тебе необходимо авторизоваться в нашем боте через VK, перейдя по ссылке '
							  . 'https://oauth.vk.com/authorize?client_id='
							  . config('app.vk_app_id')
							  . '&redirect_uri=https://trollbattle.ru/vk/auth/'
							  . '&display=page&scope=271488&response_type=code&v=5.68'
							  . '&state=' . $userId
							  . PHP_EOL . 'После этого еще раз набери команду /battle'

				]
			);
		}
		else
		{
			$user->name    = $telegramUser->getUsername();
			$user->chat_id = $chatId;

			if (!is_null(Battler::where('user_id', $userId)->first()))
			{
				$this->replyWithMessage(
					[
						'text' => $user->name . ', ты и так уже будешь биться, Потерпи...'
					]
				);
			}
			else if (!is_null(Watcher::where('user_id', $userId)->first()))
			{
				$this->replyWithMessage(
					[
						'text' => $user->name . ', ты зритель! Теперь только смотреть...'
					]
				);
			}
			else
			{

				$client = new \GuzzleHttp\Client();

				try
				{
					$res = $client->get(
						'https://api.vk.com/method/users.get',
						[
							'query' =>
								[
									'fields'       => 'about,activities,bdate,books,career,city,country,education,'
													  . 'followers_count,games,home_town,interests,movies,music,'
													  . 'personal,quotes,sex',
									'access_token' => $user->vk_token,
									'v'            => '5.68',
								]
						]
					);
				}
				catch (ClientException $exception)
				{
					$this->replyWithMessage(
						[
							'text' => 'Произошла ошибка при получение твоих данных, попробуй еще раз позже!'
						]
					);
				}

				if ($res->getStatusCode() == 200)
				{
					$data = json_decode($res->getBody()->getContents(), true);

					$info = $data['response'][0];
					unset($info['id']);

					$user->info = json_encode($info);
					$user->save();

					$battler = new Battler(['user_id' => $userId]);
					$battler->save();

					$this->replyWithMessage(
						[
							'text' => $user->name . ', ты в игре! Идет подбор соперника...'
						]
					);
				}
				else
				{
					$this->replyWithMessage(
						[
							'text' => 'Произошла ошибка при получение твоих данных, попробуй еще раз позже!'
						]
					);
				}
			}
		}
	}
}
