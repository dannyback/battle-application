<?php

namespace App\Telegram\Command;

use App\Group;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class BroadcastCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "broadcast";

	/**
	 * @var string Command Description
	 */
	protected $description = "Освещать битвы в текущей группе";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		/** @var  \Telegram\Bot\Objects\Message $message */
		$message = $this->getUpdate()->get('message');
		$chatId  = $message->getChat()->getId();

		$groupsCount = Group::count();
		if ($groupsCount > 0)
		{
			$this->replyWithMessage(
				[
					'text' => 'На данный момент только одна группа может освещать события поединка, '
							  . 'группа уже зарегистрировалась.'
				]
			);
		}
		else
		{
			$group = new Group(['chat_id' => $chatId]);
			$group->save();

			$this->replyWithMessage(
				[
					'text' => 'Теперь ваша группа будет освещать события поединка.'
				]
			);
		}
	}
}
