<?php

namespace App\Telegram\Command;

use App\Battle\Models\Battle;
use App\Group;
use App\User;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class HandleTextCommand extends Command
{
	/**
	 * @var string Command Name
	 */
	protected $name = "handle_text";

	/**
	 * @var string Command Description
	 */
	protected $description = "Псевдо команда для обработки текста от пользователя";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		/** @var  \Telegram\Bot\Objects\Message $message */
		$message      = $this->getUpdate()->get('message');
		$telegramUser = $message->getFrom();
		$userId       = $telegramUser->getId();

		/** @var User $user */
		$user = User::where('id', $userId)->first();
		if (!is_null($user))
		{

			if (!is_null($battle = Battle::where('red_user_id', $userId)->first()))
			{
				if ($battle->step === 1)
				{
					$enemy = User::where('id', $battle->blue_user_id)->first();
					\Telegram::sendMessage(
						[
							'chat_id' => $enemy->chat_id,
							'text'    => $user->name . ': ' . $message->getText()
						]
					);
				}
			}
			else if (!is_null($battle = Battle::where('blue_user_id', $userId)->first()))
			{
				if ($battle->step === 2)
				{
					$enemy = User::where('id', $battle->red_user_id)->first();
					\Telegram::sendMessage(
						[
							'chat_id' => $enemy->chat_id,
							'text'    => $user->name . ': ' . $message->getText()
						]
					);
				}
			}

			if (isset($enemy) && !is_null($enemy))
			{
				/** @var Group $group */
				$group = Group::first();
				if (!is_null($group))
				{
					\Telegram::sendMessage(
						[
							'chat_id' => $group->chat_id,
							'text'    => $user->name . ': ' . $message->getText()
						]
					);
				}
			}
		}
	}
}
