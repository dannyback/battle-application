<?php

namespace App\Telegram\Service;

use App\Battle\Models\Battle;
use App\Group;
use Carbon\Carbon;

class BattleService
{
	private static $map = [
		'about'           => 'О себе',
		'first_name'      => 'Имя',
		'last_name'       => 'Фамилия',
		'sex'             => 'Пол',
		'bdate'           => 'Дата рождения',
		'city'            => 'Город',
		'country'         => 'Страна',
		'followers_count' => 'Кол-во подписчиков',
		'games'           => 'Игры',
		'books'           => 'Книги',
		'quotes'          => 'Цитаты',
		'personal'        => 'Персональные данные'
	];

	public static function startBattle(Battle $battle)
	{
		$firstPlayerChatId = $battle->redBattler->chat_id;
		$firstPlayerInfo   = json_decode($battle->redBattler->info, true);
		$firstPlayerTown   = $firstPlayerInfo['home_town'] ?? 'Ниоткуда';
		$firstPlayerName   = $battle->redBattler->name;

		$secondPlayerChatId = $battle->blueBattler->chat_id;
		$secondPlayerInfo   = json_decode($battle->blueBattler->info, true);
		$secondPlayerTown   = $secondPlayerInfo['home_town'] ?? 'Ниоткуда';
		$secondPlayerName   = $battle->blueBattler->name;

		/** @var Group $group */
		$group = Group::first();
		if (!is_null($group))
		{
			$groupChatId = $group->chat_id;
		}
		else
		{
			$groupChatId = 0;
		}

		$playersTexts = [
			[
				$firstPlayerChatId  => "Твой соперник: $secondPlayerName. Порви его!",
				$secondPlayerChatId => "Твой соперник: $firstPlayerName. Порви его!",
				$groupChatId        => 'Супер, все готовы.' . PHP_EOL
									   . 'Бьются ' . $battle->redBattler->name . " из $firstPlayerTown и "
									   . $battle->blueBattler->name . " из $secondPlayerTown"
			],
			[
				$firstPlayerChatId  => 'Ребята голосуют, кто лучший. Ведешь себя подло и переходишь грань, отправят тебя в бан.',
				$secondPlayerChatId => 'Ребята голосуют, кто лучший. Ведешь себя подло и переходишь грань, отправят тебя в бан.',
				$groupChatId        => 'Голосуйте, кто лучший. Ведет себя подло, переходит грань — жаль, не жалей.'
			],
			[
				$firstPlayerChatId  => 'Честный бой, жесткая борьба.' . PHP_EOL
									   . 'Не материться, родственников не трогать — имей совесть.',
				$secondPlayerChatId => 'Честный бой, жесткая борьба.' . PHP_EOL
									   . 'Не материться, родственников не трогать — имей совесть.',
				$groupChatId        => 'Честный бой, жесткая борьба.' . PHP_EOL
									   . 'Не материться, родственников не трогать — имей совесть.'
			],
			[
				$firstPlayerChatId  => 'У каждого есть минута, чтобы написать убойный панч. Не успел — извини, друг, смена хода. '
									   . 'Всего три раунда по два хода.',
				$secondPlayerChatId => 'У каждого есть минута, чтобы написать убойный панч. Не успел — извини, друг, смена хода. '
									   . 'Всего три раунда по два хода.',
				$groupChatId        => 'У каждого есть минута, чтобы написать убойный панч. Не успел — извини, друг, смена хода. '
									   . 'Всего три раунда по два хода.'
			],
			[
				$firstPlayerChatId  => 'Первый ход — твой через 3, 2, 1...',
				$secondPlayerChatId => "Первым ходит $firstPlayerName через 3, 2, 1...",
				$groupChatId        => "У меня все. Начинает $firstPlayerName через 3, 2, 1"
			],
			[
				$firstPlayerChatId  => 'Минута пошла',
				$secondPlayerChatId => 'Минута пошла',
				$groupChatId        => 'Минута пошла'
			],
			[
				$firstPlayerChatId  => '1 раунд',
				$secondPlayerChatId => '1 раунд',
				$groupChatId        => '1 раунд',
			]
		];

		foreach ($playersTexts as $texts)
		{
			foreach ($texts as $chatId => $text)
			{
				if ($chatId !== 0)
				{
					\Telegram::sendMessage(
						[
							'chat_id' => $chatId,
							'text'    => $text
						]
					);
				}
			}
			sleep(1);
		}
	}

	public static function manageBattle(Battle $battle)
	{
		$map  = self::$map;
		$keys = array_keys($map);
		$key  = $keys[$battle->round - 1];


		$firstPlayerChatId = $battle->redBattler->chat_id;
		$firstPlayerName   = $battle->redBattler->name;
		$firstPlayerInfo   = json_decode($battle->redBattler->info, true);
		if (empty($firstPlayerInfo[$key]))
		{
			$name            = $map[$keys[$battle->round - 1 + 6]];
			$firstPlayerInfo = $name . ': ' . print_r($firstPlayerInfo[$keys[$battle->round - 1 + 6]] ?? '', true);;
		}
		else
		{
			$name            = $map[$keys[$battle->round - 1]];
			$firstPlayerInfo = $name . ': ' . print_r($firstPlayerInfo[$keys[$battle->round - 1]] ?? '', true);;
		}


		$secondPlayerName   = $battle->blueBattler->name;
		$secondPlayerChatId = $battle->blueBattler->chat_id;
		$secondPlayerInfo   = json_decode($battle->blueBattler->info, true);
		if (empty($secondPlayerInfo[$key]))
		{
			$name             = $map[$keys[$battle->round - 1 + 6]];
			$secondPlayerInfo = $name . ': ' . print_r($secondPlayerInfo[$keys[$battle->round - 1 + 6]] ?? '', true);
		}
		else
		{
			$name             = $map[$keys[$battle->round - 1]];
			$secondPlayerInfo = $name . ': ' . print_r($secondPlayerInfo[$keys[$battle->round - 1]] ?? '', true);;
		}

		/** @var Group $group */
		$group = Group::first();
		if (!is_null($group))
		{
			$groupChatId = $group->chat_id;
		}
		else
		{
			$groupChatId = 0;
		}


		if ($battle->round === 3 && $battle->step === 2)
		{
			$battle->delete();

			$playersTexts = [
				[
					$firstPlayerChatId  => "Бой окончен",
					$secondPlayerChatId => "Бой окончен",
					$groupChatId        => "Бой окончен"
				],
				[
					$secondPlayerChatId => "Про $firstPlayerName: $firstPlayerInfo",
					$groupChatId        => "Про $firstPlayerName: $firstPlayerInfo"
				]
			];

			foreach ($playersTexts as $texts)
			{
				foreach ($texts as $chatId => $text)
				{
					if ($chatId !== 0)
					{
						\Telegram::sendMessage(
							[
								'chat_id' => $chatId,
								'text'    => $text
							]
						);
					}
				}
			}

			return;
		}


		if ($battle->updated_at->addSeconds(60)->lt(Carbon::now()))
		{
			if ($battle->step === 1)
			{
				$playersTexts = [
					[
						$firstPlayerChatId  => "Сейчас ходит $secondPlayerName",
						$secondPlayerChatId => "Твой ход",
						$groupChatId        => "Сейчас ходит $secondPlayerName"
					],
					[
						$secondPlayerChatId => "Про $firstPlayerName:\n$firstPlayerInfo",
						$groupChatId        => "Про $firstPlayerName:\n$firstPlayerInfo"
					]
				];

				foreach ($playersTexts as $texts)
				{
					foreach ($texts as $chatId => $text)
					{
						if ($chatId !== 0)
						{
							\Telegram::sendMessage(
								[
									'chat_id' => $chatId,
									'text'    => $text
								]
							);
						}
					}
					sleep(1);
				}

				$battle->step = 2;
				$battle->save();
			}
			elseif ($battle->step === 2)
			{
				$round        = $battle->round;
				$playersTexts = [
					[
						$firstPlayerChatId  => "$round раунд окончен",
						$secondPlayerChatId => "$round раунд окончен",
						$groupChatId        => "$round раунд окончен"
					],
					[
						$firstPlayerChatId  => 'Идет голосование',
						$secondPlayerChatId => 'Идет голосование',
						$groupChatId        => "Голосуйте за лучшего. Для этого наберите команду /vote [red|blue]. Red - $firstPlayerName Blue - $secondPlayerName"
					],
				];

				foreach ($playersTexts as $texts)
				{
					foreach ($texts as $chatId => $text)
					{
						if ($chatId !== 0)
						{
							\Telegram::sendMessage(
								[
									'chat_id' => $chatId,
									'text'    => $text
								]
							);
						}
					}
					sleep(1);
				}

				$battle->round = $round + 1;
				$battle->step  = 0;
				$battle->save();
			}
			elseif ($battle->step === 0)
			{
				$round        = $battle->round;
				$playersTexts = [
					[
						$firstPlayerChatId  => "$firstPlayerName: $battle->red_score очков\n$secondPlayerName: $battle->blue_score очков",
						$secondPlayerChatId => "$firstPlayerName: $battle->red_score очков\n$secondPlayerName: $battle->blue_score очков",
						$groupChatId        => "$firstPlayerName: $battle->red_score очков\n$secondPlayerName: $battle->blue_score очков"
					],
					[
						$firstPlayerChatId  => "$round раунд",
						$secondPlayerChatId => "$round раунд",
						$groupChatId        => "$round раунд"
					],
					[
						$firstPlayerChatId => "Про $secondPlayerName:\n$secondPlayerInfo",
						$groupChatId       => "Про $secondPlayerName:\n$secondPlayerInfo"
					]
				];

				foreach ($playersTexts as $texts)
				{
					foreach ($texts as $chatId => $text)
					{
						if ($chatId !== 0)
						{
							\Telegram::sendMessage(
								[
									'chat_id' => $chatId,
									'text'    => $text
								]
							);
						}
					}
					sleep(1);
				}

				$battle->step = 1;
				$battle->save();
			}
		}
	}
}
