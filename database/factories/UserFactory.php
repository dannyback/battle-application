<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$i = 100;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(App\User::class, function (Faker $faker) use (&$i) {
    return [
        'id' => $i++,
        'name' => $faker->name,
    ];
});

$factory->define(\App\Battle\Models\Battler::class, function (Faker $faker) {
    return [
        'user_id' => 0,
    ];
});

$factory->define(\App\Battle\Models\Watcher::class, function (Faker $faker) {
    return [
        'user_id' => 0,
    ];
});
