<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('red_user_id');
            $table->unsignedInteger('blue_user_id');
            $table->unsignedInteger('round')->default(1);
            $table->unsignedInteger('step')->default(1);
            $table->unsignedInteger('red_score')->default(0);
            $table->unsignedInteger('blue_score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battles');
    }
}
