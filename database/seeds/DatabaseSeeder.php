<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\User::class, 2)->create();
        foreach ($users as $user) {
            factory(\App\Battle\Models\Battler::class)->create([
                'user_id' => $user->id,
            ]);
        }

        $watchers = factory(App\User::class, 100)->create();
        foreach ($watchers as $watcher) {
            factory(\App\Battle\Models\Watcher::class)->create([
                'user_id' => $watcher->id,
            ]);
        }
    }
}
